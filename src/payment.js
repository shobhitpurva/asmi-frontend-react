import React, {useState} from 'react';
import StripeCheckout from 'react-stripe-checkout';




function payment() {

    const makePayment = token => {
        const body = {
            token
        };
        const headers = {
            "content-Type": "application/json"
        };

        return fetch('charge', {
            method: "POST",
            headers,
            body: JSON.stringify(body)
        })
         .then(response => {
            console.log("RESPONSE ", response);
            alert("Success! Check your email for details");
            // response.redirect('/');
         })
         .catch(error => console.log(error))
    }

    return (
        <div>
            <h1>Welcome to payment page</h1>
            <StripeCheckout 
            stripeKey="pk_test_orC3gRfxKijfAbeupeFu37o700IGVnbq0H"
            token={makePayment}
            name="Buy React">

            </StripeCheckout>
        </div>
    );
}

export default payment;
