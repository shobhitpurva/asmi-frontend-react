import React from 'react';
import './App.css';
import payment from './payment';
import connect from './connect';
import {Route, Link} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Route exact path="/payment" component={payment} />
      <Route exact path="/" component={connect} />
    </div>
  );
}

export default App;


// fetch("charge", {
//     method: 'POST',
//   // body: JSON.stringify(before),
//   headers:{
//     'Content-Type': 'application/json'
//   }
// }).then(res => res.json())
// // .then(response => success(response))
// // .catch(error => failure(error));












