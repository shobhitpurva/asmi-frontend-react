import React from 'react';


class connect extends React.Component {
    constructor(props) {
      super(props);
      this.state = { apiResponse: "" };
    }
  
    callAPI() {
      let elmButton = document.querySelector("#submit");
  
      if (elmButton) {
        elmButton.addEventListener(
          "click",
          e => {
            elmButton.setAttribute("disabled", "disabled");
  
            fetch("get-oauth-link", {
              method: "GET",
              header: {
                "Content-Type": "application/json"
              }
            })
              .then(response => response.json())
              .then(data => {
                console.log(data);
                if (data.url) {
                  window.location = data.url;
                }
                else {
                  elmButton.removeAttribute("disabled");
                  elmButton.textContent = "<Something went wrong>";
                  console.log("data", data);
                }
              });
          },
        )
      }
  
    }
  
    componentWillMount() {
      this.callAPI();
    }
  
    render() {
      return (
        <div>
          <button onClick={this.callAPI} id="submit"><img src="btn.png"></img></button>
          <h1>Connect with Stripe</h1>
        </div>
      )
    }
  }
  export default connect;